﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Milestone_4
{
    public partial class Form2 : Form
    {
        public string[] x = new string[7];
        public Inventory_Item xyz = new Inventory_Item();
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        public void CheckBox_Click(object sender, EventArgs e)
        {
            try
            {
                xyz.Name = nameBox.Text;
                xyz.Description = descBox.Text;
                xyz.Expdate = expBox.Text;
                xyz.Fullstock = int.Parse(fullBox.Text);
                xyz.Pnum = int.Parse(pBox.Text);
                xyz.Price = decimal.Parse(priceBox.Text);
                xyz.Stock = int.Parse(stockBox.Text);
            }
            catch (Exception ex)

            {
                MessageBox.Show(ex.ToString());
            }
            this.Close();
        }

        public void XBox_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void StockBox_TextChanged(object sender, EventArgs e)
        {

        }

       /* public string[] TheValue()
        {
            x[0] = nameBox.Text;
            x[1] = pBox.Text;
            x[2] = stockBox.Text;
            x[3] = fullBox.Text;
            x[4] = priceBox.Text;
            x[5] = expBox.Text;
            x[6] = descBox.Text;
            return x;
        }*/
    }
}
