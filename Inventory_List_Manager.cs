﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_117_Milestone_4
{
    class Inventory_List_Manager
    {
        private List<Inventory_Item> invList = new List<Inventory_Item>();
        private Random rand = new Random();

        public void Add(Inventory_Item xxx)
        {
            invList.Add(xxx);
        }
        //add string
        public void Add(string x)
        {
            Inventory_Item tmp = new Inventory_Item();
            tmp.Name = x;
            tmp.Description = "Item description has not yet been added.";
            tmp.Expdate = "00/00/0000";
            tmp.Fullstock = 0;
            tmp.Pnum = rand.Next(100000000, 999999999);
            tmp.Price = 0.00m;
            tmp.Stock = 0;
            invList.Add(tmp);

        }

        public void Replace(string x, Inventory_Item y)
        {
            if (invList.Any())
            {
                for (int i = 0; i < invList.Count; i++)
                {
                    if (invList[i].Name.Equals(x))
                    {
                        //invList.Insert(invList.IndexOf(invList[i]), y);
                        invList.Insert(i, y);
                        invList.Remove(invList[i+1]);
                        
                    }
                }
            }
        }

        //remove based on name
        public void Remove(string x)
        {
            if (invList.Any())
            {
                foreach (var item in invList)
                {
                    if (item.Name.Equals(x))
                    {
                        invList.Remove(item);
                        break;
                    }
                }
            }
        }

        //restock function

        public void Restock(string x)
        {
            Inventory_Item restock = this.Search(x);
            if (invList.Contains(restock))
            {
                restock.Stock = restock.Fullstock;
            }
        }

        //search by name
        public Inventory_Item Search(string x)
        {
            if (!x.First().Equals('#') && !x.First().Equals('f'))
            {
                foreach (var item in invList)
                {
                    if (item.Name.Equals(x))
                    {
                        return item;
                    }
                }
            }
            else if (x.First().Equals('#'))
            {
                int productnum = int.Parse(x.Substring(1));

                foreach (var item in invList)
                {
                    if (item.Pnum == productnum)
                    {
                        return item;
                    }
                }
            }
            else if (x.First().Equals('f'))
            {
                int full = int.Parse(x.Substring(1));
                foreach (var item in invList)
                {
                    if (item.Fullstock == full)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        //search by stock
        public Inventory_Item Search(int x)
        {
            foreach (var item in invList)
            {
                if (item.Stock == x)
                {
                    return item;
                }
            }
            return null;
        }

        public string Display(string x)
        {
            var disp = this.Search(x);
            return disp.Name + " is an item with the following properties: P/N: " + disp.Pnum + ", Current Stock: " + disp.Stock + 
                ", Full Stock: " + disp.Fullstock + ", Price: " + disp.Price + ", Expiration Date: " + disp.Expdate + ", and a description of: " + disp.Description;

        }

        public int Length()
        {
            return invList.Count;
        }


    }

}
